
package granja;
import javax.swing.JOptionPane;

public class Granja {   
    
    public static void main(String[] args) {
      ejercicioGranja();      
    }
    
    public static void ejercicioGranja()
    {
        int[][] recolecta = new int[5][3];
        for (int i=0; i<5 ; i++)
        {
            for (int j=0; j<3;j++)
            {
                int dia = i+1;
                switch (j){                    
                    case 0:
                        recolecta[i][j]=Integer.parseInt(JOptionPane.showInputDialog("Escriba el numero de Naranjas del dia: " + dia));
                        break;
                    case 1:
                        recolecta[i][j]=Integer.parseInt(JOptionPane.showInputDialog("Escriba el numero de Guayabas del dia: " + dia));
                        break;
                    case 2:
                        recolecta[i][j]=Integer.parseInt(JOptionPane.showInputDialog("Escriba el numero de Manzanas del dia: " + dia));
                        break;                        
                }      
            }
        }
        float sumaNaranja=0;
        float sumaGuayaba=0;
        float sumaManzanas=0;
        int mayor=0;
        int diaMayor=0;
        for (int i=0; i<5 ; i++)
        {
            int totalDia = 0;
            for (int j=0; j<3;j++)
            {                
                switch (j){                    
                    case 0:
                        sumaNaranja= sumaNaranja + recolecta[i][j];
                        break;
                    case 1:
                        sumaGuayaba= sumaGuayaba + recolecta[i][j];
                        break;
                    case 2:
                        sumaManzanas= sumaManzanas + recolecta[i][j];
                        break;                        
                }
                totalDia = totalDia + recolecta[i][j];                
            } 
            if (i==0)
            {
                mayor = totalDia;
                diaMayor = i;
            }
            if (totalDia>mayor)
            {
                mayor = totalDia;
                diaMayor = i;
            }
            totalDia=0;
        }
        float promedioNaranja = sumaNaranja / 5;
        float promedioGuayaba = sumaGuayaba / 5;
        float promedioManzana = sumaManzanas / 5;
        diaMayor=diaMayor+1;
        JOptionPane.showMessageDialog(null, "El promedio de Naranjas recolectadas es de: " + promedioNaranja);
        JOptionPane.showMessageDialog(null, "El promedio de Guayabas recolectadas es de: " + promedioGuayaba);
        JOptionPane.showMessageDialog(null, "El promedio de Manzanas recolectadas es de: " + promedioManzana);
        JOptionPane.showMessageDialog(null, "El dia que se recolecto mas frutas fue el dia: " + diaMayor);
        
    }
    
}
